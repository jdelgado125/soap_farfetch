﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using FarFetch.Models;
using FarFetch.Controllers;
using FarFetch.Interfaces;
using System.IO;
using APIStock;
using System.Text;
using System.Xml.Serialization;

namespace FarFetch
{
    internal class Program
    {
        //static string _version = System.Reflection.Assembly.GetExecutingAssembly()
        //    .GetName()
        //    .Version
        //    .ToString();
        //static string _process = String.Empty;
        //static string _destination = String.Empty;
        //static string _source = String.Empty;

        public static async Task Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            //create service provider
            var serviceProvider = services.BuildServiceProvider();

            //entry to run app
            //await serviceProvider.GetService<App>().Run(args);

            var fileHandler = serviceProvider.GetRequiredService<IFileHandler>();
            var files = fileHandler.GetAllFiles();

            var logger = serviceProvider.GetService<ILogger<Program>>();

            foreach (string file in files)
            {
                Console.WriteLine($"Filename: {file}");
                switch (fileHandler.GetFileType(file))
                {
                    case "CS":
                        await RetrieveStockInfo(serviceProvider, fileHandler, file);
                        break;
                    case "GOBD":
                        await RetrieveOrdersByDate(serviceProvider, fileHandler, file);
                        break;
                    case "GOH":
                        await RetrieveOrderHeaders(serviceProvider, fileHandler, file);
                        break;
                    case "GOR":
                        await RetrieveOrderRows(serviceProvider, fileHandler, file);
                        break;
                    case "PKG":
                        await DecidePackaging(serviceProvider, fileHandler, file);
                        break;
                    case "RBS":
                        await RecommendedBoxSize(serviceProvider, fileHandler, file);
                        break;
                    default:
                        logger.LogError("Please provide a valid file type!");
                        //Console.WriteLine("Please provide a valid file type!");
                        break;
                }
            }

        }

        public static void ConfigureServices(IServiceCollection services)
        {
            //configure logging
            services.AddLogging(builder =>
            {
                builder.SetMinimumLevel(LogLevel.Debug);
                builder.AddConsole();
                builder.AddDebug();
                if(Environment.OSVersion.Platform != PlatformID.Unix)
                    builder.AddEventLog();
            });

            //Get the current environment
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            //build config
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environment}.json", true, true)
                .AddEnvironmentVariables()
                .Build();

            services.Configure<AppConfig>(config.GetSection(nameof(AppConfig)));
            services.Configure<SoapOptions>(config.GetSection("SoapOptions"));
            services.Configure<FileSettings>(config.GetSection("FilePaths"));
            services.Configure<ApiStockOptions>(config.GetSection("ApiStock"));
            services.Configure<WsFarfetchOptions>(config.GetSection("WsFarFetch"));

            //add app
            services.AddTransient<IApiStock, ApiStockController>();
            services.AddTransient<IWsFarFetch, WsFarfetchController>();
            services.AddTransient<IFileHandler, FileHandlerController>();
            //services.AddTransient<App>();
        }

        //static void DisplayHelp()
        //{
        //    Console.Clear();
        //    Console.WriteLine($"Version: {_version}");
        //    Console.WriteLine($"Environment Version: {Environment.Version}");
        //    Console.Write(Resources.Help);
        //    Console.Write("\n\nPress any key to continue . . .");
        //    Console.ReadKey();
        //    Environment.Exit(0);
        //}

        private static IApiStock GetClient(IServiceProvider services)
        {
            using (IServiceScope serviceScope = services.CreateScope())
            {
                IServiceProvider provider = serviceScope.ServiceProvider;
                return provider.GetRequiredService<IApiStock>();
            }  
        }

        private static IWsFarFetch GetWSClient(IServiceProvider services)
        {
            using (IServiceScope serviceScope = services.CreateScope())
            {
                IServiceProvider provider = serviceScope.ServiceProvider;
                return provider.GetRequiredService<IWsFarFetch>();
            }
        }

        public static async Task RetrieveOrderHeaders(IServiceProvider services, IFileHandler fileHandler, string filepath)
        {
            var client = GetClient(services);
            var lines = fileHandler.ReadFile(filepath);
            var data = lines[0].Split('|');

            try
            {
                GenericCollectionResponseOfOrderWorkflowStepCommonStoreHeaderDTO orders;

                if (data.Length > 1)
                {
                    string key = data[1];
                    orders = await client.OrderHeaders(data[0], key);
                }
                else
                {
                    orders = await client.OrderHeaders(data[0]);
                }

                var xml = fileHandler.ToXML(orders);
                await fileHandler.CreateFile(xml, fileHandler.GetCurrentFilename());

                MoveFileTo(filepath, fileHandler, "archive");
            }
            catch
            {
                MoveFileTo(filepath, fileHandler, "error");
            }
        }

        public static async Task RetrieveOrdersByDate(IServiceProvider services, IFileHandler fileHandler, string filepath)
        {
            var client = GetClient(services);
            var data = fileHandler.ReadFile(filepath);
            var dates = data[0].Split('|');
            string strStartDate = dates[0], strEndDate = dates[1];

            DateTime startDate = Convert.ToDateTime(strStartDate);
            DateTime endDate = Convert.ToDateTime(strEndDate);

            try
            {
                GetOrdersByDateResponse orders;
                if (dates.Length == 3)
                {
                    string key = dates[2];
                    orders = await client.GetCancelledOrders(startDate, endDate, key);
                }
                else
                {
                    orders = await client.GetCancelledOrders(startDate, endDate);
                }

                var xml = fileHandler.ToXML(orders);
                await fileHandler.CreateFile(xml, fileHandler.GetCurrentFilename());

                MoveFileTo(filepath, fileHandler, "archive");
            }
            catch
            {
                MoveFileTo(filepath, fileHandler, "error");
            }
        }

        public static async Task RetrieveOrderRows(IServiceProvider services, IFileHandler fileHandler, string filepath)
        {
            var client = GetClient(services);
            var lines = fileHandler.ReadFile(filepath);
            var data = lines[0].Split('|');
            GenericCollectionResponseOfOrderWorkflowStepCommonStoreRowDTO rows;

            try
            {
                if (data.Length > 1)
                {
                    string key = data[1];
                    rows = await client.GetOrderPartials(Convert.ToInt32(data[0]), key);
                }
                else
                {
                    rows = await client.GetOrderPartials(Convert.ToInt32(data[0]));
                }

                var xml = fileHandler.ToXML(rows);
                await fileHandler.CreateFile(xml, fileHandler.GetCurrentFilename());

                MoveFileTo(filepath, fileHandler, "archive");
            }catch
            {
                MoveFileTo(filepath, fileHandler, "error");
            }
        }

        public static async Task RetrieveStockInfo(IServiceProvider services, IFileHandler fileHandler, string filepath)
        {
            var client = GetClient(services);
            var lines = fileHandler.ReadFile(filepath);
            var data = lines[0].Split('|');
            bool stock;
            int orderLineId = Convert.ToInt32(data[0]);
            int orderId = Convert.ToInt32(data[1]);
            int status = Convert.ToInt32(data[2]);

            try
            {
                if (data.Length > 3)
                {
                    string key = data[3];
                    stock = await client.CheckStock(orderLineId, orderId, status, key);
                }
                else
                {
                    stock = await client.CheckStock(orderLineId, orderId, status);
                }

                var xml = fileHandler.ToXML(stock);
                await fileHandler.CreateFile(xml, fileHandler.GetCurrentFilename());

                MoveFileTo(filepath, fileHandler, "archive");
            }
            catch
            {
                MoveFileTo(filepath, fileHandler, "error");
            }
        }

        public static async Task RecommendedBoxSize(IServiceProvider services, IFileHandler fileHandler, string filepath)
        {
            var client = GetWSClient(services);
            var lines = fileHandler.ReadFile(filepath);
            var data = lines[0].Split('|');

            try
            {
                string box = "";

                if (data.Length > 2)
                {
                    (string, string, string) rb = (data[0], data[1], data[2]);
                    var (propertyType, itemID, key) = rb;
                    box = await client.RecommendedBox(itemID, propertyType, key);
                }
                else
                {
                    (string, string) rb = (data[0], data[1]);
                    var (propertyType, itemID) = rb;
                    box = await client.RecommendedBox(itemID, propertyType);
                }

                var xml = fileHandler.ToXML(box);
                await fileHandler.CreateFile(xml, fileHandler.GetCurrentFilename());

                MoveFileTo(filepath, fileHandler, "archive");
            }
            catch
            {
                MoveFileTo(filepath, fileHandler, "error");
            }
        }

        public static async Task DecidePackaging(IServiceProvider services, IFileHandler fileHandler, string filepath)
        {
            List<int> lineId, boxId, qty;
            List<string> desc;
            string key = "";
            bool package = false;
            var client = GetClient(services);
            var lines = fileHandler.ReadFile(filepath);
            var hdrData = lines[0].Split('|');

            try
            {
                if (hdrData.Length > 5)
                {
                    key = hdrData[5];
                }

                int orderID = Convert.ToInt32(hdrData[0]);
                lineId = new List<int>(lines.Length);
                boxId = new List<int>(lines.Length);
                qty = new List<int>(lines.Length);
                desc = new List<string>(lines.Length);

                if (lines.Length > 1)
                {
                    foreach (var line in lines)
                    {
                        var data = line.Split('|');

                        lineId.Add(Convert.ToInt32(data[1]));
                        boxId.Add(Convert.ToInt32(data[2]));
                        desc.Add(data[3]);
                        qty.Add(Convert.ToInt32(data[4]));
                    }

                    package = await client.OrderPackaging(orderID, lineId.ToArray(), boxId.ToArray(), desc.ToArray(), qty.ToArray(), key);
                }
                else
                {
                    lineId.Add(Convert.ToInt32(hdrData[1]));
                    boxId.Add(Convert.ToInt32(hdrData[2]));
                    desc.Add(hdrData[3]);
                    qty.Add(Convert.ToInt32(hdrData[4]));

                    package = await client.OrderPackaging(orderID, lineId.ToArray(), boxId.ToArray(), desc.ToArray(), qty.ToArray());
                }

                var xml = fileHandler.ToXML(package);
                await fileHandler.CreateFile(xml, fileHandler.GetCurrentFilename());

                MoveFileTo(filepath, fileHandler, "archive");
            }
            catch (Exception)
            {
                MoveFileTo(filepath, fileHandler, "error");
            }
        }

        private static void MoveFileTo(string filepath, IFileHandler fileHandler, string dirFlag)
        {
            int retry = 3;
            while (retry != 0)
            {
                if (fileHandler.MoveFile(filepath, dirFlag)) break;
                retry--;
            }
        }
    }
}
