﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIStock;

namespace FarFetch.Interfaces
{
    public interface IApiStock
    {
        Task<APIStockSoapClient> GetInstanceAsync();
        Task<GetAllItemsWithStockResponse> GetAllItemsWithStock(string key = "def");
        Task<BarcodeProcessAbsoluteQuantityResponse> DeltaStockAdjustment(string barcode, int absoluteQty, bool isAdjustment, int currentStock, string key = "def");
        Task<GenericCollectionResponseOfOrderWorkflowStepCommonStoreHeaderDTO> OrderHeaders(string step, string key = "def");
        Task<GenericCollectionResponseOfOrderWorkflowStepCommonStoreRowDTO> GetOrderPartials(int orderId, string key = "def");
        Task<bool> CheckStock(int orderLineId, int orderId, int stockStatus, string key = "def");
        Task<bool> OrderPackaging(int orderID, int[] lineId, int[] boxId, string[] desc, int[] qty, string key = "def");
        Task<GetOrdersByDateResponse> GetCancelledOrders(DateTime startDate, DateTime endDate, string key = "def");
        Task<ReturnOrderDto[]> GetPendingReturns(string key = "def");
        Task<bool> ValidateReturns(int orderId, string status, string key = "def");
    }
}
