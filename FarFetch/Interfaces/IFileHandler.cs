﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarFetch.Interfaces
{
    public interface IFileHandler
    {
        IEnumerable<string> GetAllFiles();
        string[] ReadFile(string filePath);
        Task CreateFile(string data, string filename);
        string GetFileType(string filepath);
        string GetCurrentFilename();
        string ToXML(object serializableObject);
        bool MoveFile(string filepath, string dirFlag = "");
    }
}
