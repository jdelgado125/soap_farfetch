﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarFetch.Interfaces
{
    interface IWsFarFetch
    {
        Task<WsFarFetch.WsFarfetchSoapClient> GetInstanceAsync();
        Task<string> RecommendedBox(string itemID, string propertyType, string key = "def");
    }
}
