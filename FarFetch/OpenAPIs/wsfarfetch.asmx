<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://tempuri.org/" xmlns:s1="http://microsoft.com/wsdl/types/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" targetNamespace="http://tempuri.org/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://tempuri.org/">
      <s:import namespace="http://microsoft.com/wsdl/types/" />
      <s:element name="UpdateBoArtigosRefFar">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="key" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="itemId" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="UpdateBoArtigosRefFarResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="UpdateBoArtigosRefFarResult" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPossibleDuplicates">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="brandId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="designerId" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ProductSummary">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ItemId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="FriendlyName" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="ShortDescription" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="SizeRangeId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="BrandId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="StyleId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="DesignerColorId" nillable="true" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="MainColorId" nillable="true" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="GenderId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="SeasonId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="CategoryId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="ProductLink" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="SizeRange" type="tns:SizeRange" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="SizeRange">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Acronym" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="MinimumSizePos" nillable="true" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="MaximumSizePos" nillable="true" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="FriendlyName" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="CountryCode" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="ParentId" nillable="true" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Sizes" type="tns:ArrayOfItemSize" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfItemSize">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="ItemSize" nillable="true" type="tns:ItemSize" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ItemSize">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Position" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfProductSummary">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="ProductSummary" nillable="true" type="tns:ProductSummary" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetPossibleDuplicatesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetPossibleDuplicatesResult" type="tns:ArrayOfProductSummary" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SaveGeoBrandPrices">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="brandPriceInputs" type="tns:ArrayOfGeoBrandPriceInput" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfGeoBrandPriceInput">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="GeoBrandPriceInput" nillable="true" type="tns:GeoBrandPriceInput" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="GeoBrandPriceInput">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="SeasonId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="BrandId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="DesignerId" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="RegionId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="Price" type="s:decimal" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="GeoBrandPriceSaveResult">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="OriginalInput" type="tns:GeoBrandPriceInput" />
          <s:element minOccurs="1" maxOccurs="1" name="Success" type="s:boolean" />
          <s:element minOccurs="0" maxOccurs="1" name="Errors" type="tns:ArrayOfString" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfString">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="string" nillable="true" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfGeoBrandPriceSaveResult">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="GeoBrandPriceSaveResult" nillable="true" type="tns:GeoBrandPriceSaveResult" />
        </s:sequence>
      </s:complexType>
      <s:element name="SaveGeoBrandPricesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SaveGeoBrandPricesResult" type="tns:ArrayOfGeoBrandPriceSaveResult" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetBrandRegions">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="brandId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="GeoRegion">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="RegionId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="BrandId" nillable="true" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="StoreId" nillable="true" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="CurrencyId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="Type" type="s:unsignedByte" />
          <s:element minOccurs="1" maxOccurs="1" name="IsMandatory" type="s:boolean" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfGeoRegion">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="GeoRegion" nillable="true" type="tns:GeoRegion" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetBrandRegionsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetBrandRegionsResult" type="tns:ArrayOfGeoRegion" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AssociateToChildProduct">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="itemId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="boutiquePrice" type="s:decimal" />
            <s:element minOccurs="0" maxOccurs="1" name="barcodes" type="tns:ArrayOfSimpleBoutiqueBarcode" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfSimpleBoutiqueBarcode">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="SimpleBoutiqueBarcode" nillable="true" type="tns:SimpleBoutiqueBarcode" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="SimpleBoutiqueBarcode">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="SizePosition" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Barcode" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="AssociationResult">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="ItemId" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="SizeScaleId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="SizeScaleName" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Success" type="s:boolean" />
          <s:element minOccurs="0" maxOccurs="1" name="ErrorMessages" type="tns:ArrayOfString" />
          <s:element minOccurs="0" maxOccurs="1" name="Barcodes" type="tns:ArrayOfBoutiqueBarcodeResult" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfBoutiqueBarcodeResult">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="BoutiqueBarcodeResult" nillable="true" type="tns:BoutiqueBarcodeResult" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="BoutiqueBarcodeResult">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="SizePosition" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="SizeValue" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="StoreBarcode" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="FarfetchBarcode" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="AssociateToChildProductResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="AssociateToChildProductResult" type="tns:AssociationResult" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AssociateProduct">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="itemId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="boutiquePrice" type="s:decimal" />
            <s:element minOccurs="0" maxOccurs="1" name="barcodes" type="tns:ArrayOfSimpleBoutiqueBarcode" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AssociateProductResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="AssociateProductResult" type="tns:AssociationResult" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AssociateProductWithVat">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="itemId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="boutiquePrice" type="s:decimal" />
            <s:element minOccurs="0" maxOccurs="1" name="barcodes" type="tns:ArrayOfSimpleBoutiqueBarcode" />
            <s:element minOccurs="1" maxOccurs="1" name="vatPercentualException" nillable="true" type="s:double" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AssociateProductWithVatResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="AssociateProductWithVatResult" type="tns:AssociationResult" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllStock">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllStockResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllStockResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllStockWithStoreBarcodes">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllStockWithStoreBarcodesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllStockWithStoreBarcodesResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetBoxName">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="id" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetBoxNameResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetBoxNameResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetBoxes">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="location" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetBoxesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetBoxesResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetDefaultCountries">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetDefaultCountriesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetDefaultCountriesResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetList">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="propertie" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetListResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetListResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetColourTypes">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ArticleId" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="safekey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetColourTypesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetColourTypesResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetComposition">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ArticleId" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="safekey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetCompositionResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetCompositionResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMainView">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="ItemID" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMainViewResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetMainViewResult" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="ItemID" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMeasures">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ArticleId" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMeasuresResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetMeasuresResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPropValueNameById">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="id" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPropValueNameByIdResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetPropValueNameByIdResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPropValueIdByName">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="name" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="propid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPropValueIdByNameResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetPropValueIdByNameResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetProperty">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="propertyType" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ItemID" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPropertyResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetPropertyResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSizeHelpId">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="genderid" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="catid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSizeHelpIdResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetSizeHelpIdResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSizes">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="sizeRangeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSizesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetSizesResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetStock">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeid" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="Barcode" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetStockResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetStockResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetStoreItems">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="storeId" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetStoreItemsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetStoreItemsResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetStorePrice">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeid" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="ItemID" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Currency" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="StorePrice" type="s:double" />
            <s:element minOccurs="1" maxOccurs="1" name="WebsitePrice" type="s:double" />
            <s:element minOccurs="0" maxOccurs="1" name="safekey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetStorePriceResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetStorePriceResult" type="s:boolean" />
            <s:element minOccurs="1" maxOccurs="1" name="StorePrice" type="s:double" />
            <s:element minOccurs="1" maxOccurs="1" name="WebsitePrice" type="s:double" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetWebsiteDescription">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="ItemID" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetWebsiteDescriptionResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetWebsiteDescriptionResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="existSKU">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeid" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="skuid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="existSKUResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="existSKUResult" type="s:boolean" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="isBrandVisible">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="brandid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="isBrandVisibleResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="isBrandVisibleResult" type="s:boolean" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSKUandDesingerID">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="FFitemId" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="SKU" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSKUandDesingerIDResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetSKUandDesingerIDResult" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SKU" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetParentItemId">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="itemId" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetParentItemIdResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetParentItemIdResult" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetFFItemId">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="DesignerId" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SKU" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetFFItemIdResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetFFItemIdResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetFFItemUrl">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="storeid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SpecialSafeKey" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="urlPartner" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetFFItemUrlResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetFFItemUrlResult" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="urlPartner" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetGender">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetGenderResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetGenderResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMaterialCategory">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMaterialCategoryResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetMaterialCategoryResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMaterial">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Material" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetMaterialResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetMaterialResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetCategories">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetCategoriesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetCategoriesResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="Get2ndLevelCats">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="FirstLevelCat" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="Get2ndLevelCatsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Get2ndLevelCatsResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSizeRangesFiltered">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="genderid" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="catid" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSizeRangesFilteredResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetSizeRangesFilteredResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SearchSizeScalesForProductCreation">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="categoryId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="brandId" nillable="true" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SearchSizeScalesForProductCreationResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SearchSizeScalesForProductCreationResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSlots">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSlotsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetSlotsResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetOpenSlots">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetOpenSlotsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetOpenSlotsResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSonId">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ParentId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSonIdResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetSonIdResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSonIdForOrderManagement">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ParentId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSonIdForOrderManagementResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetSonIdForOrderManagementResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetDesignerIdsForSeasonBrand">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="brandId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="seasonId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetDesignerIdsForSeasonBrandResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetDesignerIdsForSeasonBrandResult" type="tns:ArrayOfString" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="IsBrandSeasonDesignerIdListBound">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="brandId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="seasonId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="IsBrandSeasonDesignerIdListBoundResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="IsBrandSeasonDesignerIdListBoundResult" type="s:boolean" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateProduct">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="storeID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="slotID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="seasonId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="collectionId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="genderId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="brandId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="itemCategory" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="style" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="price" type="s:double" />
            <s:element minOccurs="1" maxOccurs="1" name="materialID" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="colour" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="boxID" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="escalaID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="itemName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="detail" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="itemDescription" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="stylingNotes" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="designerInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="additionalInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="fitAndSizeInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="sizesQtd" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="SecLevelCatId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="transportationProperties" type="tns:ArrayOfInt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfInt">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="int" type="s:int" />
        </s:sequence>
      </s:complexType>
      <s:element name="CreateProductResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="CreateProductResult" type="s:boolean" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateProductWithUuid">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="uuid" type="s1:guid" />
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="storeID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="slotID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="seasonId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="collectionId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="genderId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="brandId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="itemCategory" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="style" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="price" type="s:double" />
            <s:element minOccurs="1" maxOccurs="1" name="materialID" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="colour" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="boxID" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="escalaID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="itemName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="detail" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="itemDescription" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="stylingNotes" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="designerInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="additionalInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="fitAndSizeInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="sizesQtd" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="SecLevelCatId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="transportationProperties" type="tns:ArrayOfInt" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateProductWithUuidResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="CreateProductWithUuidResult" type="s:boolean" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateProductWithUuidAndVat">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="uuid" type="s1:guid" />
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="storeID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="slotID" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="seasonId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="collectionId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="genderId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="brandId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="itemCategory" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="style" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="price" type="s:double" />
            <s:element minOccurs="1" maxOccurs="1" name="materialID" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="colour" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="boxID" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="escalaID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="itemName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="detail" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="itemDescription" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="stylingNotes" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="designerInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="additionalInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="fitAndSizeInfo" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="sizesQtd" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="SecLevelCatId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="transportationProperties" type="tns:ArrayOfInt" />
            <s:element minOccurs="1" maxOccurs="1" name="vatPercentualException" nillable="true" type="s:double" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateProductWithUuidAndVatResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="CreateProductWithUuidAndVatResult" type="s:boolean" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="InsertMaterials">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="materialID" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="compositionType" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="percentage" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="InsertMaterialsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="InsertMaterialsResult" type="s:boolean" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="InsertSizesQuantity">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="articleId" type="s:long" />
            <s:element minOccurs="1" maxOccurs="1" name="sizeRangeId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="size" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="quantity" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="barcode" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="InsertSizesQuantityResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="InsertSizesQuantityResult" type="s:boolean" />
            <s:element minOccurs="0" maxOccurs="1" name="barcode" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllPictures">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ItemID" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllPicturesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllPicturesResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllPicturesByBarcode">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Barcode" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="Size" type="tns:PhotoSizes" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:simpleType name="PhotoSizes">
        <s:restriction base="s:string">
          <s:enumeration value="Small" />
          <s:enumeration value="Medium" />
          <s:enumeration value="Large" />
          <s:enumeration value="Max" />
        </s:restriction>
      </s:simpleType>
      <s:element name="GetAllPicturesByBarcodeResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllPicturesByBarcodeResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="InsertStoreBarcode">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ItemId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="Pos" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="Barcode" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="InsertStoreBarcodeResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="InsertStoreBarcodeResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ProductPriceUpdate">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="storeId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="articleId" nillable="true" type="s:long" />
            <s:element minOccurs="0" maxOccurs="1" name="barcode" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="sku" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="price" nillable="true" type="s:decimal" />
            <s:element minOccurs="1" maxOccurs="1" name="discount" nillable="true" type="s:decimal" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ProductPriceUpdateResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ProductPriceUpdateResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AddCountryOfOriginToItem">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ItemId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="Country" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AddCountryOfOriginToItemResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="AddCountryOfOriginToItemResult" type="s:boolean" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AddDesignerIdToBrandSeasonList">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="brandid" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="seasonId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="designerIds" type="tns:ArrayOfString" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AddDesignerIdToBrandSeasonListResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="AddDesignerIdToBrandSeasonListResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllSizeRangesInUse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="StoreId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="SafeKey" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllSizeRangesInUseResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetAllSizeRangesInUseResult">
              <s:complexType>
                <s:sequence>
                  <s:any minOccurs="0" maxOccurs="unbounded" namespace="http://www.w3.org/2001/XMLSchema" processContents="lax" />
                  <s:any minOccurs="1" namespace="urn:schemas-microsoft-com:xml-diffgram-v1" processContents="lax" />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="MappDuplicates">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="safeKey" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="parentStoreId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="parentId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="childStoreId" type="s:int" />
            <s:element minOccurs="1" maxOccurs="1" name="childId" type="s:int" />
            <s:element minOccurs="0" maxOccurs="1" name="errorMessage" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="MappDuplicatesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="MappDuplicatesResult" type="s:boolean" />
            <s:element minOccurs="0" maxOccurs="1" name="errorMessage" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetTransportationProperties">
        <s:complexType />
      </s:element>
      <s:complexType name="TransportationProperty">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="Description" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="PropertyId" type="s:int" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfTransportationProperty">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="TransportationProperty" nillable="true" type="tns:TransportationProperty" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetTransportationPropertiesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetTransportationPropertiesResult" type="tns:ArrayOfTransportationProperty" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetProductTransportationProperties">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="productId" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="TransportationProduct">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="ProductId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Properties" type="tns:ArrayOfTransportationProperty" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetProductTransportationPropertiesResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetProductTransportationPropertiesResult" type="tns:TransportationProduct" />
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
    <s:schema elementFormDefault="qualified" targetNamespace="http://microsoft.com/wsdl/types/">
      <s:simpleType name="guid">
        <s:restriction base="s:string">
          <s:pattern value="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}" />
        </s:restriction>
      </s:simpleType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="UpdateBoArtigosRefFarSoapIn">
    <wsdl:part name="parameters" element="tns:UpdateBoArtigosRefFar" />
  </wsdl:message>
  <wsdl:message name="UpdateBoArtigosRefFarSoapOut">
    <wsdl:part name="parameters" element="tns:UpdateBoArtigosRefFarResponse" />
  </wsdl:message>
  <wsdl:message name="GetPossibleDuplicatesSoapIn">
    <wsdl:part name="parameters" element="tns:GetPossibleDuplicates" />
  </wsdl:message>
  <wsdl:message name="GetPossibleDuplicatesSoapOut">
    <wsdl:part name="parameters" element="tns:GetPossibleDuplicatesResponse" />
  </wsdl:message>
  <wsdl:message name="SaveGeoBrandPricesSoapIn">
    <wsdl:part name="parameters" element="tns:SaveGeoBrandPrices" />
  </wsdl:message>
  <wsdl:message name="SaveGeoBrandPricesSoapOut">
    <wsdl:part name="parameters" element="tns:SaveGeoBrandPricesResponse" />
  </wsdl:message>
  <wsdl:message name="GetBrandRegionsSoapIn">
    <wsdl:part name="parameters" element="tns:GetBrandRegions" />
  </wsdl:message>
  <wsdl:message name="GetBrandRegionsSoapOut">
    <wsdl:part name="parameters" element="tns:GetBrandRegionsResponse" />
  </wsdl:message>
  <wsdl:message name="AssociateToChildProductSoapIn">
    <wsdl:part name="parameters" element="tns:AssociateToChildProduct" />
  </wsdl:message>
  <wsdl:message name="AssociateToChildProductSoapOut">
    <wsdl:part name="parameters" element="tns:AssociateToChildProductResponse" />
  </wsdl:message>
  <wsdl:message name="AssociateProductSoapIn">
    <wsdl:part name="parameters" element="tns:AssociateProduct" />
  </wsdl:message>
  <wsdl:message name="AssociateProductSoapOut">
    <wsdl:part name="parameters" element="tns:AssociateProductResponse" />
  </wsdl:message>
  <wsdl:message name="AssociateProductWithVatSoapIn">
    <wsdl:part name="parameters" element="tns:AssociateProductWithVat" />
  </wsdl:message>
  <wsdl:message name="AssociateProductWithVatSoapOut">
    <wsdl:part name="parameters" element="tns:AssociateProductWithVatResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllStockSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllStock" />
  </wsdl:message>
  <wsdl:message name="GetAllStockSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllStockResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllStockWithStoreBarcodesSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllStockWithStoreBarcodes" />
  </wsdl:message>
  <wsdl:message name="GetAllStockWithStoreBarcodesSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllStockWithStoreBarcodesResponse" />
  </wsdl:message>
  <wsdl:message name="GetBoxNameSoapIn">
    <wsdl:part name="parameters" element="tns:GetBoxName" />
  </wsdl:message>
  <wsdl:message name="GetBoxNameSoapOut">
    <wsdl:part name="parameters" element="tns:GetBoxNameResponse" />
  </wsdl:message>
  <wsdl:message name="GetBoxesSoapIn">
    <wsdl:part name="parameters" element="tns:GetBoxes" />
  </wsdl:message>
  <wsdl:message name="GetBoxesSoapOut">
    <wsdl:part name="parameters" element="tns:GetBoxesResponse" />
  </wsdl:message>
  <wsdl:message name="GetDefaultCountriesSoapIn">
    <wsdl:part name="parameters" element="tns:GetDefaultCountries" />
  </wsdl:message>
  <wsdl:message name="GetDefaultCountriesSoapOut">
    <wsdl:part name="parameters" element="tns:GetDefaultCountriesResponse" />
  </wsdl:message>
  <wsdl:message name="GetListSoapIn">
    <wsdl:part name="parameters" element="tns:GetList" />
  </wsdl:message>
  <wsdl:message name="GetListSoapOut">
    <wsdl:part name="parameters" element="tns:GetListResponse" />
  </wsdl:message>
  <wsdl:message name="GetColourTypesSoapIn">
    <wsdl:part name="parameters" element="tns:GetColourTypes" />
  </wsdl:message>
  <wsdl:message name="GetColourTypesSoapOut">
    <wsdl:part name="parameters" element="tns:GetColourTypesResponse" />
  </wsdl:message>
  <wsdl:message name="GetCompositionSoapIn">
    <wsdl:part name="parameters" element="tns:GetComposition" />
  </wsdl:message>
  <wsdl:message name="GetCompositionSoapOut">
    <wsdl:part name="parameters" element="tns:GetCompositionResponse" />
  </wsdl:message>
  <wsdl:message name="GetMainViewSoapIn">
    <wsdl:part name="parameters" element="tns:GetMainView" />
  </wsdl:message>
  <wsdl:message name="GetMainViewSoapOut">
    <wsdl:part name="parameters" element="tns:GetMainViewResponse" />
  </wsdl:message>
  <wsdl:message name="GetMeasuresSoapIn">
    <wsdl:part name="parameters" element="tns:GetMeasures" />
  </wsdl:message>
  <wsdl:message name="GetMeasuresSoapOut">
    <wsdl:part name="parameters" element="tns:GetMeasuresResponse" />
  </wsdl:message>
  <wsdl:message name="GetPropValueNameByIdSoapIn">
    <wsdl:part name="parameters" element="tns:GetPropValueNameById" />
  </wsdl:message>
  <wsdl:message name="GetPropValueNameByIdSoapOut">
    <wsdl:part name="parameters" element="tns:GetPropValueNameByIdResponse" />
  </wsdl:message>
  <wsdl:message name="GetPropValueIdByNameSoapIn">
    <wsdl:part name="parameters" element="tns:GetPropValueIdByName" />
  </wsdl:message>
  <wsdl:message name="GetPropValueIdByNameSoapOut">
    <wsdl:part name="parameters" element="tns:GetPropValueIdByNameResponse" />
  </wsdl:message>
  <wsdl:message name="GetPropertySoapIn">
    <wsdl:part name="parameters" element="tns:GetProperty" />
  </wsdl:message>
  <wsdl:message name="GetPropertySoapOut">
    <wsdl:part name="parameters" element="tns:GetPropertyResponse" />
  </wsdl:message>
  <wsdl:message name="GetSizeHelpIdSoapIn">
    <wsdl:part name="parameters" element="tns:GetSizeHelpId" />
  </wsdl:message>
  <wsdl:message name="GetSizeHelpIdSoapOut">
    <wsdl:part name="parameters" element="tns:GetSizeHelpIdResponse" />
  </wsdl:message>
  <wsdl:message name="GetSizesSoapIn">
    <wsdl:part name="parameters" element="tns:GetSizes" />
  </wsdl:message>
  <wsdl:message name="GetSizesSoapOut">
    <wsdl:part name="parameters" element="tns:GetSizesResponse" />
  </wsdl:message>
  <wsdl:message name="GetStockSoapIn">
    <wsdl:part name="parameters" element="tns:GetStock" />
  </wsdl:message>
  <wsdl:message name="GetStockSoapOut">
    <wsdl:part name="parameters" element="tns:GetStockResponse" />
  </wsdl:message>
  <wsdl:message name="GetStoreItemsSoapIn">
    <wsdl:part name="parameters" element="tns:GetStoreItems" />
  </wsdl:message>
  <wsdl:message name="GetStoreItemsSoapOut">
    <wsdl:part name="parameters" element="tns:GetStoreItemsResponse" />
  </wsdl:message>
  <wsdl:message name="GetStorePriceSoapIn">
    <wsdl:part name="parameters" element="tns:GetStorePrice" />
  </wsdl:message>
  <wsdl:message name="GetStorePriceSoapOut">
    <wsdl:part name="parameters" element="tns:GetStorePriceResponse" />
  </wsdl:message>
  <wsdl:message name="GetWebsiteDescriptionSoapIn">
    <wsdl:part name="parameters" element="tns:GetWebsiteDescription" />
  </wsdl:message>
  <wsdl:message name="GetWebsiteDescriptionSoapOut">
    <wsdl:part name="parameters" element="tns:GetWebsiteDescriptionResponse" />
  </wsdl:message>
  <wsdl:message name="existSKUSoapIn">
    <wsdl:part name="parameters" element="tns:existSKU" />
  </wsdl:message>
  <wsdl:message name="existSKUSoapOut">
    <wsdl:part name="parameters" element="tns:existSKUResponse" />
  </wsdl:message>
  <wsdl:message name="isBrandVisibleSoapIn">
    <wsdl:part name="parameters" element="tns:isBrandVisible" />
  </wsdl:message>
  <wsdl:message name="isBrandVisibleSoapOut">
    <wsdl:part name="parameters" element="tns:isBrandVisibleResponse" />
  </wsdl:message>
  <wsdl:message name="GetSKUandDesingerIDSoapIn">
    <wsdl:part name="parameters" element="tns:GetSKUandDesingerID" />
  </wsdl:message>
  <wsdl:message name="GetSKUandDesingerIDSoapOut">
    <wsdl:part name="parameters" element="tns:GetSKUandDesingerIDResponse" />
  </wsdl:message>
  <wsdl:message name="GetParentItemIdSoapIn">
    <wsdl:part name="parameters" element="tns:GetParentItemId" />
  </wsdl:message>
  <wsdl:message name="GetParentItemIdSoapOut">
    <wsdl:part name="parameters" element="tns:GetParentItemIdResponse" />
  </wsdl:message>
  <wsdl:message name="GetFFItemIdSoapIn">
    <wsdl:part name="parameters" element="tns:GetFFItemId" />
  </wsdl:message>
  <wsdl:message name="GetFFItemIdSoapOut">
    <wsdl:part name="parameters" element="tns:GetFFItemIdResponse" />
  </wsdl:message>
  <wsdl:message name="GetFFItemUrlSoapIn">
    <wsdl:part name="parameters" element="tns:GetFFItemUrl" />
  </wsdl:message>
  <wsdl:message name="GetFFItemUrlSoapOut">
    <wsdl:part name="parameters" element="tns:GetFFItemUrlResponse" />
  </wsdl:message>
  <wsdl:message name="GetGenderSoapIn">
    <wsdl:part name="parameters" element="tns:GetGender" />
  </wsdl:message>
  <wsdl:message name="GetGenderSoapOut">
    <wsdl:part name="parameters" element="tns:GetGenderResponse" />
  </wsdl:message>
  <wsdl:message name="GetMaterialCategorySoapIn">
    <wsdl:part name="parameters" element="tns:GetMaterialCategory" />
  </wsdl:message>
  <wsdl:message name="GetMaterialCategorySoapOut">
    <wsdl:part name="parameters" element="tns:GetMaterialCategoryResponse" />
  </wsdl:message>
  <wsdl:message name="GetMaterialSoapIn">
    <wsdl:part name="parameters" element="tns:GetMaterial" />
  </wsdl:message>
  <wsdl:message name="GetMaterialSoapOut">
    <wsdl:part name="parameters" element="tns:GetMaterialResponse" />
  </wsdl:message>
  <wsdl:message name="GetCategoriesSoapIn">
    <wsdl:part name="parameters" element="tns:GetCategories" />
  </wsdl:message>
  <wsdl:message name="GetCategoriesSoapOut">
    <wsdl:part name="parameters" element="tns:GetCategoriesResponse" />
  </wsdl:message>
  <wsdl:message name="Get2ndLevelCatsSoapIn">
    <wsdl:part name="parameters" element="tns:Get2ndLevelCats" />
  </wsdl:message>
  <wsdl:message name="Get2ndLevelCatsSoapOut">
    <wsdl:part name="parameters" element="tns:Get2ndLevelCatsResponse" />
  </wsdl:message>
  <wsdl:message name="GetSizeRangesFilteredSoapIn">
    <wsdl:part name="parameters" element="tns:GetSizeRangesFiltered" />
  </wsdl:message>
  <wsdl:message name="GetSizeRangesFilteredSoapOut">
    <wsdl:part name="parameters" element="tns:GetSizeRangesFilteredResponse" />
  </wsdl:message>
  <wsdl:message name="SearchSizeScalesForProductCreationSoapIn">
    <wsdl:part name="parameters" element="tns:SearchSizeScalesForProductCreation" />
  </wsdl:message>
  <wsdl:message name="SearchSizeScalesForProductCreationSoapOut">
    <wsdl:part name="parameters" element="tns:SearchSizeScalesForProductCreationResponse" />
  </wsdl:message>
  <wsdl:message name="GetSlotsSoapIn">
    <wsdl:part name="parameters" element="tns:GetSlots" />
  </wsdl:message>
  <wsdl:message name="GetSlotsSoapOut">
    <wsdl:part name="parameters" element="tns:GetSlotsResponse" />
  </wsdl:message>
  <wsdl:message name="GetOpenSlotsSoapIn">
    <wsdl:part name="parameters" element="tns:GetOpenSlots" />
  </wsdl:message>
  <wsdl:message name="GetOpenSlotsSoapOut">
    <wsdl:part name="parameters" element="tns:GetOpenSlotsResponse" />
  </wsdl:message>
  <wsdl:message name="GetSonIdSoapIn">
    <wsdl:part name="parameters" element="tns:GetSonId" />
  </wsdl:message>
  <wsdl:message name="GetSonIdSoapOut">
    <wsdl:part name="parameters" element="tns:GetSonIdResponse" />
  </wsdl:message>
  <wsdl:message name="GetSonIdForOrderManagementSoapIn">
    <wsdl:part name="parameters" element="tns:GetSonIdForOrderManagement" />
  </wsdl:message>
  <wsdl:message name="GetSonIdForOrderManagementSoapOut">
    <wsdl:part name="parameters" element="tns:GetSonIdForOrderManagementResponse" />
  </wsdl:message>
  <wsdl:message name="GetDesignerIdsForSeasonBrandSoapIn">
    <wsdl:part name="parameters" element="tns:GetDesignerIdsForSeasonBrand" />
  </wsdl:message>
  <wsdl:message name="GetDesignerIdsForSeasonBrandSoapOut">
    <wsdl:part name="parameters" element="tns:GetDesignerIdsForSeasonBrandResponse" />
  </wsdl:message>
  <wsdl:message name="IsBrandSeasonDesignerIdListBoundSoapIn">
    <wsdl:part name="parameters" element="tns:IsBrandSeasonDesignerIdListBound" />
  </wsdl:message>
  <wsdl:message name="IsBrandSeasonDesignerIdListBoundSoapOut">
    <wsdl:part name="parameters" element="tns:IsBrandSeasonDesignerIdListBoundResponse" />
  </wsdl:message>
  <wsdl:message name="CreateProductSoapIn">
    <wsdl:part name="parameters" element="tns:CreateProduct" />
  </wsdl:message>
  <wsdl:message name="CreateProductSoapOut">
    <wsdl:part name="parameters" element="tns:CreateProductResponse" />
  </wsdl:message>
  <wsdl:message name="CreateProductWithUuidSoapIn">
    <wsdl:part name="parameters" element="tns:CreateProductWithUuid" />
  </wsdl:message>
  <wsdl:message name="CreateProductWithUuidSoapOut">
    <wsdl:part name="parameters" element="tns:CreateProductWithUuidResponse" />
  </wsdl:message>
  <wsdl:message name="CreateProductWithUuidAndVatSoapIn">
    <wsdl:part name="parameters" element="tns:CreateProductWithUuidAndVat" />
  </wsdl:message>
  <wsdl:message name="CreateProductWithUuidAndVatSoapOut">
    <wsdl:part name="parameters" element="tns:CreateProductWithUuidAndVatResponse" />
  </wsdl:message>
  <wsdl:message name="InsertMaterialsSoapIn">
    <wsdl:part name="parameters" element="tns:InsertMaterials" />
  </wsdl:message>
  <wsdl:message name="InsertMaterialsSoapOut">
    <wsdl:part name="parameters" element="tns:InsertMaterialsResponse" />
  </wsdl:message>
  <wsdl:message name="InsertSizesQuantitySoapIn">
    <wsdl:part name="parameters" element="tns:InsertSizesQuantity" />
  </wsdl:message>
  <wsdl:message name="InsertSizesQuantitySoapOut">
    <wsdl:part name="parameters" element="tns:InsertSizesQuantityResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllPicturesSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllPictures" />
  </wsdl:message>
  <wsdl:message name="GetAllPicturesSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllPicturesResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllPicturesByBarcodeSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllPicturesByBarcode" />
  </wsdl:message>
  <wsdl:message name="GetAllPicturesByBarcodeSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllPicturesByBarcodeResponse" />
  </wsdl:message>
  <wsdl:message name="InsertStoreBarcodeSoapIn">
    <wsdl:part name="parameters" element="tns:InsertStoreBarcode" />
  </wsdl:message>
  <wsdl:message name="InsertStoreBarcodeSoapOut">
    <wsdl:part name="parameters" element="tns:InsertStoreBarcodeResponse" />
  </wsdl:message>
  <wsdl:message name="ProductPriceUpdateSoapIn">
    <wsdl:part name="parameters" element="tns:ProductPriceUpdate" />
  </wsdl:message>
  <wsdl:message name="ProductPriceUpdateSoapOut">
    <wsdl:part name="parameters" element="tns:ProductPriceUpdateResponse" />
  </wsdl:message>
  <wsdl:message name="AddCountryOfOriginToItemSoapIn">
    <wsdl:part name="parameters" element="tns:AddCountryOfOriginToItem" />
  </wsdl:message>
  <wsdl:message name="AddCountryOfOriginToItemSoapOut">
    <wsdl:part name="parameters" element="tns:AddCountryOfOriginToItemResponse" />
  </wsdl:message>
  <wsdl:message name="AddDesignerIdToBrandSeasonListSoapIn">
    <wsdl:part name="parameters" element="tns:AddDesignerIdToBrandSeasonList" />
  </wsdl:message>
  <wsdl:message name="AddDesignerIdToBrandSeasonListSoapOut">
    <wsdl:part name="parameters" element="tns:AddDesignerIdToBrandSeasonListResponse" />
  </wsdl:message>
  <wsdl:message name="GetAllSizeRangesInUseSoapIn">
    <wsdl:part name="parameters" element="tns:GetAllSizeRangesInUse" />
  </wsdl:message>
  <wsdl:message name="GetAllSizeRangesInUseSoapOut">
    <wsdl:part name="parameters" element="tns:GetAllSizeRangesInUseResponse" />
  </wsdl:message>
  <wsdl:message name="MappDuplicatesSoapIn">
    <wsdl:part name="parameters" element="tns:MappDuplicates" />
  </wsdl:message>
  <wsdl:message name="MappDuplicatesSoapOut">
    <wsdl:part name="parameters" element="tns:MappDuplicatesResponse" />
  </wsdl:message>
  <wsdl:message name="GetTransportationPropertiesSoapIn">
    <wsdl:part name="parameters" element="tns:GetTransportationProperties" />
  </wsdl:message>
  <wsdl:message name="GetTransportationPropertiesSoapOut">
    <wsdl:part name="parameters" element="tns:GetTransportationPropertiesResponse" />
  </wsdl:message>
  <wsdl:message name="GetProductTransportationPropertiesSoapIn">
    <wsdl:part name="parameters" element="tns:GetProductTransportationProperties" />
  </wsdl:message>
  <wsdl:message name="GetProductTransportationPropertiesSoapOut">
    <wsdl:part name="parameters" element="tns:GetProductTransportationPropertiesResponse" />
  </wsdl:message>
  <wsdl:portType name="WsFarfetchSoap">
    <wsdl:operation name="UpdateBoArtigosRefFar">
      <wsdl:input message="tns:UpdateBoArtigosRefFarSoapIn" />
      <wsdl:output message="tns:UpdateBoArtigosRefFarSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetPossibleDuplicates">
      <wsdl:input message="tns:GetPossibleDuplicatesSoapIn" />
      <wsdl:output message="tns:GetPossibleDuplicatesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="SaveGeoBrandPrices">
      <wsdl:input message="tns:SaveGeoBrandPricesSoapIn" />
      <wsdl:output message="tns:SaveGeoBrandPricesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetBrandRegions">
      <wsdl:input message="tns:GetBrandRegionsSoapIn" />
      <wsdl:output message="tns:GetBrandRegionsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="AssociateToChildProduct">
      <wsdl:input message="tns:AssociateToChildProductSoapIn" />
      <wsdl:output message="tns:AssociateToChildProductSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="AssociateProduct">
      <wsdl:input message="tns:AssociateProductSoapIn" />
      <wsdl:output message="tns:AssociateProductSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="AssociateProductWithVat">
      <wsdl:input message="tns:AssociateProductWithVatSoapIn" />
      <wsdl:output message="tns:AssociateProductWithVatSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllStock">
      <wsdl:input message="tns:GetAllStockSoapIn" />
      <wsdl:output message="tns:GetAllStockSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllStockWithStoreBarcodes">
      <wsdl:input message="tns:GetAllStockWithStoreBarcodesSoapIn" />
      <wsdl:output message="tns:GetAllStockWithStoreBarcodesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetBoxName">
      <wsdl:input message="tns:GetBoxNameSoapIn" />
      <wsdl:output message="tns:GetBoxNameSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetBoxes">
      <wsdl:input message="tns:GetBoxesSoapIn" />
      <wsdl:output message="tns:GetBoxesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetDefaultCountries">
      <wsdl:input message="tns:GetDefaultCountriesSoapIn" />
      <wsdl:output message="tns:GetDefaultCountriesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetList">
      <wsdl:input message="tns:GetListSoapIn" />
      <wsdl:output message="tns:GetListSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetColourTypes">
      <wsdl:input message="tns:GetColourTypesSoapIn" />
      <wsdl:output message="tns:GetColourTypesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetComposition">
      <wsdl:input message="tns:GetCompositionSoapIn" />
      <wsdl:output message="tns:GetCompositionSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetMainView">
      <wsdl:input message="tns:GetMainViewSoapIn" />
      <wsdl:output message="tns:GetMainViewSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetMeasures">
      <wsdl:input message="tns:GetMeasuresSoapIn" />
      <wsdl:output message="tns:GetMeasuresSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetPropValueNameById">
      <wsdl:input message="tns:GetPropValueNameByIdSoapIn" />
      <wsdl:output message="tns:GetPropValueNameByIdSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetPropValueIdByName">
      <wsdl:input message="tns:GetPropValueIdByNameSoapIn" />
      <wsdl:output message="tns:GetPropValueIdByNameSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetProperty">
      <wsdl:input message="tns:GetPropertySoapIn" />
      <wsdl:output message="tns:GetPropertySoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSizeHelpId">
      <wsdl:input message="tns:GetSizeHelpIdSoapIn" />
      <wsdl:output message="tns:GetSizeHelpIdSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSizes">
      <wsdl:input message="tns:GetSizesSoapIn" />
      <wsdl:output message="tns:GetSizesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetStock">
      <wsdl:input message="tns:GetStockSoapIn" />
      <wsdl:output message="tns:GetStockSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetStoreItems">
      <wsdl:input message="tns:GetStoreItemsSoapIn" />
      <wsdl:output message="tns:GetStoreItemsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetStorePrice">
      <wsdl:input message="tns:GetStorePriceSoapIn" />
      <wsdl:output message="tns:GetStorePriceSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetWebsiteDescription">
      <wsdl:input message="tns:GetWebsiteDescriptionSoapIn" />
      <wsdl:output message="tns:GetWebsiteDescriptionSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="existSKU">
      <wsdl:input message="tns:existSKUSoapIn" />
      <wsdl:output message="tns:existSKUSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="isBrandVisible">
      <wsdl:input message="tns:isBrandVisibleSoapIn" />
      <wsdl:output message="tns:isBrandVisibleSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSKUandDesingerID">
      <wsdl:input message="tns:GetSKUandDesingerIDSoapIn" />
      <wsdl:output message="tns:GetSKUandDesingerIDSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetParentItemId">
      <wsdl:input message="tns:GetParentItemIdSoapIn" />
      <wsdl:output message="tns:GetParentItemIdSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetFFItemId">
      <wsdl:input message="tns:GetFFItemIdSoapIn" />
      <wsdl:output message="tns:GetFFItemIdSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetFFItemUrl">
      <wsdl:input message="tns:GetFFItemUrlSoapIn" />
      <wsdl:output message="tns:GetFFItemUrlSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetGender">
      <wsdl:input message="tns:GetGenderSoapIn" />
      <wsdl:output message="tns:GetGenderSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetMaterialCategory">
      <wsdl:input message="tns:GetMaterialCategorySoapIn" />
      <wsdl:output message="tns:GetMaterialCategorySoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetMaterial">
      <wsdl:input message="tns:GetMaterialSoapIn" />
      <wsdl:output message="tns:GetMaterialSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetCategories">
      <wsdl:input message="tns:GetCategoriesSoapIn" />
      <wsdl:output message="tns:GetCategoriesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="Get2ndLevelCats">
      <wsdl:input message="tns:Get2ndLevelCatsSoapIn" />
      <wsdl:output message="tns:Get2ndLevelCatsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSizeRangesFiltered">
      <wsdl:input message="tns:GetSizeRangesFilteredSoapIn" />
      <wsdl:output message="tns:GetSizeRangesFilteredSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="SearchSizeScalesForProductCreation">
      <wsdl:input message="tns:SearchSizeScalesForProductCreationSoapIn" />
      <wsdl:output message="tns:SearchSizeScalesForProductCreationSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSlots">
      <wsdl:input message="tns:GetSlotsSoapIn" />
      <wsdl:output message="tns:GetSlotsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetOpenSlots">
      <wsdl:input message="tns:GetOpenSlotsSoapIn" />
      <wsdl:output message="tns:GetOpenSlotsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSonId">
      <wsdl:input message="tns:GetSonIdSoapIn" />
      <wsdl:output message="tns:GetSonIdSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSonIdForOrderManagement">
      <wsdl:input message="tns:GetSonIdForOrderManagementSoapIn" />
      <wsdl:output message="tns:GetSonIdForOrderManagementSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetDesignerIdsForSeasonBrand">
      <wsdl:input message="tns:GetDesignerIdsForSeasonBrandSoapIn" />
      <wsdl:output message="tns:GetDesignerIdsForSeasonBrandSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="IsBrandSeasonDesignerIdListBound">
      <wsdl:input message="tns:IsBrandSeasonDesignerIdListBoundSoapIn" />
      <wsdl:output message="tns:IsBrandSeasonDesignerIdListBoundSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="CreateProduct">
      <wsdl:input message="tns:CreateProductSoapIn" />
      <wsdl:output message="tns:CreateProductSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="CreateProductWithUuid">
      <wsdl:input message="tns:CreateProductWithUuidSoapIn" />
      <wsdl:output message="tns:CreateProductWithUuidSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="CreateProductWithUuidAndVat">
      <wsdl:input message="tns:CreateProductWithUuidAndVatSoapIn" />
      <wsdl:output message="tns:CreateProductWithUuidAndVatSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="InsertMaterials">
      <wsdl:input message="tns:InsertMaterialsSoapIn" />
      <wsdl:output message="tns:InsertMaterialsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="InsertSizesQuantity">
      <wsdl:input message="tns:InsertSizesQuantitySoapIn" />
      <wsdl:output message="tns:InsertSizesQuantitySoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllPictures">
      <wsdl:input message="tns:GetAllPicturesSoapIn" />
      <wsdl:output message="tns:GetAllPicturesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllPicturesByBarcode">
      <wsdl:input message="tns:GetAllPicturesByBarcodeSoapIn" />
      <wsdl:output message="tns:GetAllPicturesByBarcodeSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="InsertStoreBarcode">
      <wsdl:input message="tns:InsertStoreBarcodeSoapIn" />
      <wsdl:output message="tns:InsertStoreBarcodeSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="ProductPriceUpdate">
      <wsdl:input message="tns:ProductPriceUpdateSoapIn" />
      <wsdl:output message="tns:ProductPriceUpdateSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="AddCountryOfOriginToItem">
      <wsdl:input message="tns:AddCountryOfOriginToItemSoapIn" />
      <wsdl:output message="tns:AddCountryOfOriginToItemSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="AddDesignerIdToBrandSeasonList">
      <wsdl:input message="tns:AddDesignerIdToBrandSeasonListSoapIn" />
      <wsdl:output message="tns:AddDesignerIdToBrandSeasonListSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetAllSizeRangesInUse">
      <wsdl:input message="tns:GetAllSizeRangesInUseSoapIn" />
      <wsdl:output message="tns:GetAllSizeRangesInUseSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="MappDuplicates">
      <wsdl:input message="tns:MappDuplicatesSoapIn" />
      <wsdl:output message="tns:MappDuplicatesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetTransportationProperties">
      <wsdl:input message="tns:GetTransportationPropertiesSoapIn" />
      <wsdl:output message="tns:GetTransportationPropertiesSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetProductTransportationProperties">
      <wsdl:input message="tns:GetProductTransportationPropertiesSoapIn" />
      <wsdl:output message="tns:GetProductTransportationPropertiesSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="WsFarfetchSoap" type="tns:WsFarfetchSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="UpdateBoArtigosRefFar">
      <soap:operation soapAction="http://tempuri.org/UpdateBoArtigosRefFar" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPossibleDuplicates">
      <soap:operation soapAction="http://tempuri.org/GetPossibleDuplicates" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SaveGeoBrandPrices">
      <soap:operation soapAction="http://tempuri.org/SaveGeoBrandPrices" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetBrandRegions">
      <soap:operation soapAction="http://tempuri.org/GetBrandRegions" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssociateToChildProduct">
      <soap:operation soapAction="http://tempuri.org/AssociateToChildProduct" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssociateProduct">
      <soap:operation soapAction="http://tempuri.org/AssociateProduct" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssociateProductWithVat">
      <soap:operation soapAction="http://tempuri.org/AssociateProductWithVat" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllStock">
      <soap:operation soapAction="http://tempuri.org/GetAllStock" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllStockWithStoreBarcodes">
      <soap:operation soapAction="http://tempuri.org/GetAllStockWithStoreBarcodes" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetBoxName">
      <soap:operation soapAction="http://tempuri.org/GetBoxName" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetBoxes">
      <soap:operation soapAction="http://tempuri.org/GetBoxes" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDefaultCountries">
      <soap:operation soapAction="http://tempuri.org/GetDefaultCountries" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetList">
      <soap:operation soapAction="http://tempuri.org/GetList" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetColourTypes">
      <soap:operation soapAction="http://tempuri.org/GetColourTypes" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetComposition">
      <soap:operation soapAction="http://tempuri.org/GetComposition" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMainView">
      <soap:operation soapAction="http://tempuri.org/GetMainView" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMeasures">
      <soap:operation soapAction="http://tempuri.org/GetMeasures" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPropValueNameById">
      <soap:operation soapAction="http://tempuri.org/GetPropValueNameById" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPropValueIdByName">
      <soap:operation soapAction="http://tempuri.org/GetPropValueIdByName" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProperty">
      <soap:operation soapAction="http://tempuri.org/GetProperty" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSizeHelpId">
      <soap:operation soapAction="http://tempuri.org/GetSizeHelpId" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSizes">
      <soap:operation soapAction="http://tempuri.org/GetSizes" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetStock">
      <soap:operation soapAction="http://tempuri.org/GetStock" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetStoreItems">
      <soap:operation soapAction="http://tempuri.org/GetStoreItems" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetStorePrice">
      <soap:operation soapAction="http://tempuri.org/GetStorePrice" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetWebsiteDescription">
      <soap:operation soapAction="http://tempuri.org/GetWebsiteDescription" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="existSKU">
      <soap:operation soapAction="http://tempuri.org/existSKU" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="isBrandVisible">
      <soap:operation soapAction="http://tempuri.org/isBrandVisible" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSKUandDesingerID">
      <soap:operation soapAction="http://tempuri.org/GetSKUandDesingerID" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetParentItemId">
      <soap:operation soapAction="http://tempuri.org/GetParentItemId" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFFItemId">
      <soap:operation soapAction="http://tempuri.org/GetFFItemId" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFFItemUrl">
      <soap:operation soapAction="http://tempuri.org/GetFFItemUrl" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetGender">
      <soap:operation soapAction="http://tempuri.org/GetGender" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMaterialCategory">
      <soap:operation soapAction="http://tempuri.org/GetMaterialCategory" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMaterial">
      <soap:operation soapAction="http://tempuri.org/GetMaterial" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetCategories">
      <soap:operation soapAction="http://tempuri.org/GetCategories" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Get2ndLevelCats">
      <soap:operation soapAction="http://tempuri.org/Get2ndLevelCats" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSizeRangesFiltered">
      <soap:operation soapAction="http://tempuri.org/GetSizeRangesFiltered" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SearchSizeScalesForProductCreation">
      <soap:operation soapAction="http://tempuri.org/SearchSizeScalesForProductCreation" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSlots">
      <soap:operation soapAction="http://tempuri.org/GetSlots" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetOpenSlots">
      <soap:operation soapAction="http://tempuri.org/GetOpenSlots" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSonId">
      <soap:operation soapAction="http://tempuri.org/GetSonId" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSonIdForOrderManagement">
      <soap:operation soapAction="http://tempuri.org/GetSonIdForOrderManagement" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDesignerIdsForSeasonBrand">
      <soap:operation soapAction="http://tempuri.org/GetDesignerIdsForSeasonBrand" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="IsBrandSeasonDesignerIdListBound">
      <soap:operation soapAction="http://tempuri.org/IsBrandSeasonDesignerIdListBound" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateProduct">
      <soap:operation soapAction="http://tempuri.org/CreateProduct" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateProductWithUuid">
      <soap:operation soapAction="http://tempuri.org/CreateProductWithUuid" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateProductWithUuidAndVat">
      <soap:operation soapAction="http://tempuri.org/CreateProductWithUuidAndVat" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="InsertMaterials">
      <soap:operation soapAction="http://tempuri.org/InsertMaterials" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="InsertSizesQuantity">
      <soap:operation soapAction="http://tempuri.org/InsertSizesQuantity" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllPictures">
      <soap:operation soapAction="http://tempuri.org/GetAllPictures" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllPicturesByBarcode">
      <soap:operation soapAction="http://tempuri.org/GetAllPicturesByBarcode" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="InsertStoreBarcode">
      <soap:operation soapAction="http://tempuri.org/InsertStoreBarcode" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProductPriceUpdate">
      <soap:operation soapAction="http://tempuri.org/ProductPriceUpdate" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AddCountryOfOriginToItem">
      <soap:operation soapAction="http://tempuri.org/AddCountryOfOriginToItem" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AddDesignerIdToBrandSeasonList">
      <soap:operation soapAction="http://tempuri.org/AddDesignerIdToBrandSeasonList" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllSizeRangesInUse">
      <soap:operation soapAction="http://tempuri.org/GetAllSizeRangesInUse" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="MappDuplicates">
      <soap:operation soapAction="http://tempuri.org/MappDuplicates" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTransportationProperties">
      <soap:operation soapAction="http://tempuri.org/GetTransportationProperties" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProductTransportationProperties">
      <soap:operation soapAction="http://tempuri.org/GetProductTransportationProperties" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="WsFarfetchSoap12" type="tns:WsFarfetchSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="UpdateBoArtigosRefFar">
      <soap12:operation soapAction="http://tempuri.org/UpdateBoArtigosRefFar" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPossibleDuplicates">
      <soap12:operation soapAction="http://tempuri.org/GetPossibleDuplicates" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SaveGeoBrandPrices">
      <soap12:operation soapAction="http://tempuri.org/SaveGeoBrandPrices" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetBrandRegions">
      <soap12:operation soapAction="http://tempuri.org/GetBrandRegions" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssociateToChildProduct">
      <soap12:operation soapAction="http://tempuri.org/AssociateToChildProduct" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssociateProduct">
      <soap12:operation soapAction="http://tempuri.org/AssociateProduct" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssociateProductWithVat">
      <soap12:operation soapAction="http://tempuri.org/AssociateProductWithVat" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllStock">
      <soap12:operation soapAction="http://tempuri.org/GetAllStock" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllStockWithStoreBarcodes">
      <soap12:operation soapAction="http://tempuri.org/GetAllStockWithStoreBarcodes" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetBoxName">
      <soap12:operation soapAction="http://tempuri.org/GetBoxName" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetBoxes">
      <soap12:operation soapAction="http://tempuri.org/GetBoxes" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDefaultCountries">
      <soap12:operation soapAction="http://tempuri.org/GetDefaultCountries" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetList">
      <soap12:operation soapAction="http://tempuri.org/GetList" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetColourTypes">
      <soap12:operation soapAction="http://tempuri.org/GetColourTypes" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetComposition">
      <soap12:operation soapAction="http://tempuri.org/GetComposition" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMainView">
      <soap12:operation soapAction="http://tempuri.org/GetMainView" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMeasures">
      <soap12:operation soapAction="http://tempuri.org/GetMeasures" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPropValueNameById">
      <soap12:operation soapAction="http://tempuri.org/GetPropValueNameById" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPropValueIdByName">
      <soap12:operation soapAction="http://tempuri.org/GetPropValueIdByName" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProperty">
      <soap12:operation soapAction="http://tempuri.org/GetProperty" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSizeHelpId">
      <soap12:operation soapAction="http://tempuri.org/GetSizeHelpId" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSizes">
      <soap12:operation soapAction="http://tempuri.org/GetSizes" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetStock">
      <soap12:operation soapAction="http://tempuri.org/GetStock" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetStoreItems">
      <soap12:operation soapAction="http://tempuri.org/GetStoreItems" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetStorePrice">
      <soap12:operation soapAction="http://tempuri.org/GetStorePrice" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetWebsiteDescription">
      <soap12:operation soapAction="http://tempuri.org/GetWebsiteDescription" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="existSKU">
      <soap12:operation soapAction="http://tempuri.org/existSKU" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="isBrandVisible">
      <soap12:operation soapAction="http://tempuri.org/isBrandVisible" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSKUandDesingerID">
      <soap12:operation soapAction="http://tempuri.org/GetSKUandDesingerID" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetParentItemId">
      <soap12:operation soapAction="http://tempuri.org/GetParentItemId" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFFItemId">
      <soap12:operation soapAction="http://tempuri.org/GetFFItemId" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFFItemUrl">
      <soap12:operation soapAction="http://tempuri.org/GetFFItemUrl" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetGender">
      <soap12:operation soapAction="http://tempuri.org/GetGender" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMaterialCategory">
      <soap12:operation soapAction="http://tempuri.org/GetMaterialCategory" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMaterial">
      <soap12:operation soapAction="http://tempuri.org/GetMaterial" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetCategories">
      <soap12:operation soapAction="http://tempuri.org/GetCategories" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Get2ndLevelCats">
      <soap12:operation soapAction="http://tempuri.org/Get2ndLevelCats" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSizeRangesFiltered">
      <soap12:operation soapAction="http://tempuri.org/GetSizeRangesFiltered" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SearchSizeScalesForProductCreation">
      <soap12:operation soapAction="http://tempuri.org/SearchSizeScalesForProductCreation" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSlots">
      <soap12:operation soapAction="http://tempuri.org/GetSlots" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetOpenSlots">
      <soap12:operation soapAction="http://tempuri.org/GetOpenSlots" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSonId">
      <soap12:operation soapAction="http://tempuri.org/GetSonId" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSonIdForOrderManagement">
      <soap12:operation soapAction="http://tempuri.org/GetSonIdForOrderManagement" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDesignerIdsForSeasonBrand">
      <soap12:operation soapAction="http://tempuri.org/GetDesignerIdsForSeasonBrand" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="IsBrandSeasonDesignerIdListBound">
      <soap12:operation soapAction="http://tempuri.org/IsBrandSeasonDesignerIdListBound" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateProduct">
      <soap12:operation soapAction="http://tempuri.org/CreateProduct" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateProductWithUuid">
      <soap12:operation soapAction="http://tempuri.org/CreateProductWithUuid" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateProductWithUuidAndVat">
      <soap12:operation soapAction="http://tempuri.org/CreateProductWithUuidAndVat" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="InsertMaterials">
      <soap12:operation soapAction="http://tempuri.org/InsertMaterials" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="InsertSizesQuantity">
      <soap12:operation soapAction="http://tempuri.org/InsertSizesQuantity" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllPictures">
      <soap12:operation soapAction="http://tempuri.org/GetAllPictures" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllPicturesByBarcode">
      <soap12:operation soapAction="http://tempuri.org/GetAllPicturesByBarcode" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="InsertStoreBarcode">
      <soap12:operation soapAction="http://tempuri.org/InsertStoreBarcode" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProductPriceUpdate">
      <soap12:operation soapAction="http://tempuri.org/ProductPriceUpdate" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AddCountryOfOriginToItem">
      <soap12:operation soapAction="http://tempuri.org/AddCountryOfOriginToItem" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AddDesignerIdToBrandSeasonList">
      <soap12:operation soapAction="http://tempuri.org/AddDesignerIdToBrandSeasonList" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllSizeRangesInUse">
      <soap12:operation soapAction="http://tempuri.org/GetAllSizeRangesInUse" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="MappDuplicates">
      <soap12:operation soapAction="http://tempuri.org/MappDuplicates" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTransportationProperties">
      <soap12:operation soapAction="http://tempuri.org/GetTransportationProperties" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProductTransportationProperties">
      <soap12:operation soapAction="http://tempuri.org/GetProductTransportationProperties" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="WsFarfetch">
    <wsdl:port name="WsFarfetchSoap" binding="tns:WsFarfetchSoap">
      <soap:address location="https://retail-ws.farfetch.com/ws/wsfarfetch.asmx" />
    </wsdl:port>
    <wsdl:port name="WsFarfetchSoap12" binding="tns:WsFarfetchSoap12">
      <soap12:address location="https://retail-ws.farfetch.com/ws/wsfarfetch.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>