﻿using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Collections.Generic;
using FarFetch.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using FarFetch.Interfaces;
using System.Xml.Serialization;

namespace FarFetch.Controllers
{
    public class FileHandlerController : IFileHandler
    {
        public IEnumerable<string> _files;
        public readonly IOptions<FileSettings> _filePaths;
        public readonly ILogger<FileHandlerController> _logger;
        private string currFile;

        public FileHandlerController(IOptions<FileSettings> fileOptions, ILogger<FileHandlerController> logger)
        {
            _filePaths = fileOptions;
            _logger = logger;
            _logger.LogInformation($"In directory: {_filePaths.Value.In}");
        }

        public IEnumerable<string> GetAllFiles()
        {
            if (Directory.Exists(_filePaths.Value.In))
            {
                _files = ProcessDirectory(_filePaths.Value.In);
                //Postprocess RDY files to TXT
                _files = _files.Select(f => Path.ChangeExtension(f, ".TXT")).ToList();
            }

            _logger.LogInformation($"Files found: {_files.Count()}");

            return _files;
        }

        private IEnumerable<string> ProcessDirectory(string targetDirectory)
        {
            return Directory.EnumerateFiles(targetDirectory, "*.RDY");
        }

        public string[] ReadFile(string filePath)
        {
            string fileName = Path.GetFileName(filePath);

            SetCurrentFilename(fileName);

            return File.ReadAllLines(filePath);

        }

        public bool MoveFile(string filepath, string dirFlag)
        {
            string destination = "";

            switch (dirFlag.ToLower())
            {
                case "error":
                    destination = _filePaths.Value.Error;
                    break;
                case "archive":
                    destination = _filePaths.Value.Archive;
                    break;
                case "out":
                    destination = _filePaths.Value.Out;
                    break;
                default:
                    destination = _filePaths.Value.Out;
                    break;
            }

            try
            {
                CreateDirIfNotExists(destination);
                destination += GetCurrentFilename();
                if (File.Exists(destination))
                {
                    var fileName = Path.GetFileNameWithoutExtension(destination) + "_";
                    fileName += Path.GetRandomFileName();
                    fileName = Path.GetFileNameWithoutExtension(fileName) + ".TXT";
                    destination = Path.Combine(_filePaths.Value.Error, fileName);
                }

                File.Move(filepath, destination);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }

            return true;
            
        }

        private void CreateDirIfNotExists(string path)
        {
            if (!Directory.Exists(path))
            {
                DirectoryInfo directory = Directory.CreateDirectory(path);
                _logger.LogInformation($"Directory created successfully: {directory.FullName}");
            }
        }

        public string GetCurrentFilename()
        {
            return currFile;
        }

        private void SetCurrentFilename(string filename)
        {
            currFile = filename;
        }

        private void DeleteFile(string filename)
        {
            try
            {
                File.Delete(filename);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        public string ToXML(object serializableObject)
        {
            using (var stringwriter = new StringWriter())
            {
                var serializer = new XmlSerializer(serializableObject.GetType());
                serializer.Serialize(stringwriter, serializableObject);
                return stringwriter.ToString();
            }
        }

        public async Task CreateFile(string data, string filename)
        {
            string filePath = _filePaths.Value.Out + filename;

            await File.WriteAllTextAsync(filePath, data, Encoding.UTF8);

            File.Create(Path.Combine(_filePaths.Value.Out, Path.GetFileNameWithoutExtension(filename), ".RDY"));
        }

        public string GetFileType(string filepath)
        {
            string fileName = Path.GetFileName(filepath);

            return fileName.Split('_')[0];
        }
    }
}
