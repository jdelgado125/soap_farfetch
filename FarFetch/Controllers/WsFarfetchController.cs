﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using FarFetch.Models;
using FarFetch.Interfaces;
using WsFarFetch;

namespace FarFetch.Controllers
{
    public class WsFarfetchController : IWsFarFetch
    {
        public readonly EndpointAddress _endpointAddress;
        //public readonly BasicHttpBinding _basicHttpBinding;
        public readonly IOptions<SoapOptions> _soapOptions;
        public readonly IOptions<WsFarfetchOptions> _wsFarfetchOptions;
        public readonly ILogger<WsFarfetchController> _logger;

        public WsFarfetchController(IOptions<SoapOptions> soapOptions, IOptions<WsFarfetchOptions> wsFarfetchOptions, ILogger<WsFarfetchController> logger)
        {
            _wsFarfetchOptions = wsFarfetchOptions;
            _endpointAddress = new EndpointAddress(_wsFarfetchOptions.Value.URL);
            _logger = logger;
            _soapOptions = soapOptions;

            //_basicHttpBinding = new BasicHttpBinding(_endpointAddress.Uri.Scheme.ToLower() == "http" ? 
            //    BasicHttpSecurityMode.None : BasicHttpSecurityMode.Transport);

            //_basicHttpBinding.OpenTimeout = TimeSpan.MaxValue;
            //_basicHttpBinding.CloseTimeout = TimeSpan.MaxValue;
            //_basicHttpBinding.ReceiveTimeout = TimeSpan.MaxValue;
            //_basicHttpBinding.SendTimeout = TimeSpan.MaxValue;
        }

        public async Task<WsFarfetchSoapClient> GetInstanceAsync()
        {
            return await Task.Run(() => new WsFarfetchSoapClient(WsFarfetchSoapClient.EndpointConfiguration.WsFarfetchSoap12, _endpointAddress));
        }

        public async Task<string> RecommendedBox(string itemID, string propertyType, string key)
        {
            string resp = null;
            WsFarfetchSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                resp = await client.GetPropertyAsync(propertyType, itemID, key);
                client.Close();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "RecommendedBox (RBS) request failed");
                client.Abort();
                throw;
            }

            return resp;
        }
    }
}
