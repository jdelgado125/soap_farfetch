﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using FarFetch.Models;
using FarFetch.Interfaces;
using APIStock;

namespace FarFetch.Controllers
{
    public class ApiStockController : IApiStock
    {
        public readonly EndpointAddress _endpointAddress;
        //public readonly BasicHttpBinding _basicHttpBinding;
        public readonly IOptions<SoapOptions> _soapOptions;
        public readonly IOptions<ApiStockOptions> _apiStockOptions;
        public readonly ILogger<ApiStockController> _logger;

        public ApiStockController(IOptions<SoapOptions> soapOptions, IOptions<ApiStockOptions> apiStockOptions, ILogger<ApiStockController> logger)
        {
            _apiStockOptions = apiStockOptions;
            _endpointAddress = new EndpointAddress(_apiStockOptions.Value.URL);
            _logger = logger;
            _soapOptions = soapOptions;

            //_basicHttpBinding = new BasicHttpBinding(_endpointAddress.Uri.Scheme.ToLower() == "http" ? 
            //    BasicHttpSecurityMode.None : BasicHttpSecurityMode.Transport);

            //_basicHttpBinding.OpenTimeout = TimeSpan.MaxValue;
            //_basicHttpBinding.CloseTimeout = TimeSpan.MaxValue;
            //_basicHttpBinding.ReceiveTimeout = TimeSpan.MaxValue;
            //_basicHttpBinding.SendTimeout = TimeSpan.MaxValue;
        }

        public async Task<APIStockSoapClient> GetInstanceAsync()
        {
            return await Task.Run(() => new APIStockSoapClient(APIStockSoapClient.EndpointConfiguration.APIStockSoap12, _endpointAddress));
        }

        public async Task<GetAllItemsWithStockResponse> GetAllItemsWithStock(string key)
        {
            GetAllItemsWithStockResponse resp = null;
            APIStockSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                string errorMsg = "";
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                var request = new GetAllItemsWithStockRequest(key, errorMsg);
                resp = await client.GetAllItemsWithStockAsync(request);
                client.Close();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "GetAllItemsWithStock request error");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<BarcodeProcessAbsoluteQuantityResponse> DeltaStockAdjustment(string barcode, int absoluteQty, bool isAdjustment, int currentStock, string key)
        {
            BarcodeProcessAbsoluteQuantityResponse resp = null;
            APIStockSoapClient client = null;
            string errorMsg = "";
            try
            {
                client = await GetInstanceAsync();
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                var request = new BarcodeProcessAbsoluteQuantityRequest(key, barcode, absoluteQty, errorMsg, isAdjustment, currentStock);
                resp = await client.BarcodeProcessAbsoluteQuantityAsync(request);
                client.Close();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Delta Stock Adjustment request error");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<GenericCollectionResponseOfOrderWorkflowStepCommonStoreHeaderDTO> OrderHeaders(string step, string key)
        {
            GenericCollectionResponseOfOrderWorkflowStepCommonStoreHeaderDTO resp = null;
            APIStockSoapClient client = null;
            StoreOrderWorkflowSteps flowStep = StoreOrderWorkflowSteps.Step1; //default to step1

            try
            {
                client = await GetInstanceAsync();
                switch (step)
                {
                    case "STEP1":
                        flowStep = StoreOrderWorkflowSteps.Step1;
                        break;
                    case "STEP2":
                        flowStep = StoreOrderWorkflowSteps.Step2;
                        break;
                    case "STEP3":
                        flowStep = StoreOrderWorkflowSteps.Step3;
                        break;
                    default:
                        Console.WriteLine($"{step} is not a valid step");
                        break;

                }
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                resp = await client.GetOrdersHeadersAsync(flowStep, key);
                client.Close();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GetOrderHeaders (GOH) {step} request failed");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<GenericCollectionResponseOfOrderWorkflowStepCommonStoreRowDTO> GetOrderPartials(int orderId, string key)
        {
            GenericCollectionResponseOfOrderWorkflowStepCommonStoreRowDTO resp = null;
            APIStockSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                resp = await client.GetOrdersRowsAsync(orderId, key);
                client.Close();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "GetOrdersRow (GOR) request failed");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<bool> CheckStock(int orderLineId, int orderId, int stockStatus, string key)
        {
            bool resp = false;
            APIStockSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                resp = await client.SendOrderStep1toStep2Async(key, orderLineId, orderId, stockStatus);
                client.Close();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Check stock (CS) request failed.");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<bool> OrderPackaging(int orderID, int[] lineId, int[] boxId, string[] desc, int[] qty, string key)
        {
            bool resp = false;
            APIStockSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                //OrderLine[] orderLines = null;
                List<OrderLineRequest> orderLines = new List<OrderLineRequest>();
                for(int i = 0; i < lineId.Length; i++)
                {
                    //Create the box portion of the OrderLineRequest
                    var box = new Box();
                    box.Id = boxId[i];
                    box.Description = desc[i];
                    box.Quantity = qty[i];

                    //Create the OrderLineRequest and add the Box
                    var orderLineRequest = new OrderLineRequest();
                    orderLineRequest.Id = lineId[i];
                    orderLineRequest.Box = box;

                    //Add the OrderLineRequest to the list for the SendOrder request
                    orderLines.Add(orderLineRequest);
                }

                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                //Convert the list to an array before making the request call
                resp = await client.SendOrderStep3toStep4WithPackagesAsync(key, orderID, orderLines.ToArray());
                client.Close();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "OrderPackaging (PKG) request failed");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<GetOrdersByDateResponse> GetCancelledOrders(DateTime startDate, DateTime endDate, string key)
        {
            string errMsg = "";
            GetOrdersByDateResponse resp = null;
            APIStockSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                var dateRequest = new GetOrdersByDateRequest(key, startDate, endDate, errMsg);
                resp = await client.GetOrdersByDateAsync(dateRequest);
                client.Close();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "GetCancelledOrders (GOBD) request failed");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<ReturnOrderDto[]> GetPendingReturns(string key)
        {
            ReturnOrderDto[] resp = null;
            APIStockSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                resp = await client.GetReturnsAsync(key);
                client.Close();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "GetPendingReturns request failed");
                client.Abort();
                throw;
            }

            return resp;
        }

        public async Task<bool> ValidateReturns(int orderId, string status, string key)
        {
            bool resp = false;
            APIStockSoapClient client = null;
            try
            {
                client = await GetInstanceAsync();
                var returnStatus = status switch
                {
                    "AcceptWithShippingCost" => ReturnStatus.AcceptWithShippingCost,
                    "AcceptWithoutShippingCost" => ReturnStatus.AcceptWithoutShippingCost,
                    "Refuse" => ReturnStatus.Refuse,
                    _ => throw new ArgumentException($"{status} is not a valid status"),
                };
                key = key == "def" ? _soapOptions.Value.ApiKey : key;
                resp = await client.ValidateReturnAsync(key, orderId, returnStatus);
                client.Close();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ValidateReturns request failed");
                client.Abort();
                throw;
            }

            return resp;
        }
    }
}
