﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Collections.Specialized;
using System.ServiceModel.Description;
using Microsoft.Extensions.Options;
using FarFetch.Models;
using System.ServiceModel;

namespace FarFetch
{
    class FarFetch
    {
        //private WsFarFetch.WsFarfetchSoapClient _WsFarfetchSoapClient;
        private APIStock.APIStockSoapClient _apiStockSoapClient;
        private IOptions<SoapOptions> _soapOptions;
        
        public FarFetch(IOptions<SoapOptions> soapOptions)
        {
            _soapOptions = soapOptions;
        }

        private string GetTestApiKey()
        {
            return _soapOptions.Value.TestApiKey;
        }

        private string GetProdApiKey()
        {
            return _soapOptions.Value.ProdApiKey;
        }

        private string GetApiStockURL()
        {
            return _soapOptions.Value.ApiStockURL;
        }

        private string GetWsFarfetchURL()
        {
            return _soapOptions.Value.WsFarFetchURL;
        }

        public async Task GetAllOrders()
        {
            //< Task < APIStock.GenericCollectionResponseOfOrderWorkflowStepCommonStoreHeaderDTO >>
            //Task<APIStock.GenericCollectionResponseOfOrderWorkflowStepCommonStoreHeaderDTO> response = null;
            var tcs = new TaskCompletionSource<APIStock.GenericCollectionResponseOfOrderWorkflowStepCommonStoreHeaderDTO>();
            APIStock.APIStockSoapClient client = new APIStock.APIStockSoapClient(APIStock.APIStockSoapClient.EndpointConfiguration.APIStockSoap12);

            try
            {
                //client.ClientCredentials.UserName.UserName = _soapOptions.Value.Username; //new APIStock.APIStockSoapClient(APIStock.APIStockSoapClient.EndpointConfiguration.APIStockSoap12);
                //client.ClientCredentials.UserName.Password = _soapOptions.Value.Password;
                //BasicHttpBinding binding = new BasicHttpBinding();
                //binding.Security.Mode = BasicHttpSecurityMode.Transport;
                //binding.MaxReceivedMessageSize = 100000000;
                //client.Endpoint.Binding = binding;
                //client.Endpoint.Address = new EndpointAddress(GetApiStockURL());
                var response = await client.GetOrdersHeadersAsync(APIStock.StoreOrderWorkflowSteps.Step2, GetTestApiKey());
                tcs.SetResult(response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            //return tcs.Task;
        }

        private BasicHttpBinding GetHttpBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            return binding;
        }

        private EndpointAddress GetEndpointAddress(string URL)
        {
            return new EndpointAddress(URL);
        }

    }
}
