﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarFetch.Models
{
    public class FileSettings
    {
        public string In { get; set; }
        public string Out { get; set; }
        public string Archive { get; set; }
        public string Error { get; set; }
    }
}
