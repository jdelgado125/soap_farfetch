﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarFetch.Models
{
    public class WsFarfetchOptions
    {
        public string URL { get; set; }
    }
}
